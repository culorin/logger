//
// quick hack for colors in windows consoles -> a very stripped down windows
// console coloring from the "term" crate (https://github.com/Stebalien/term)
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub struct WinConsole;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
extern crate kernel32;
extern crate winapi;

use std::io;
use std::ptr;

use super::ConsoleColor as Col;
use super::ColorConsole as Console;
// ----------------------------------------------------------------------------
fn color_to_bits(color: Col) -> u16 {
    match color {
        Col::Black => 0,
        Col::Blue => 0x1,
        Col::Green => 0x2,
        Col::Red => 0x4,
        Col::Yellow => 0x2 | 0x4,
        Col::Magenta => 0x1 | 0x4,
        Col::Cyan => 0x1 | 0x2,
        Col::White => 0x1 | 0x2 | 0x4,
        _ => unreachable!(),
    }
}
// ----------------------------------------------------------------------------
fn conout() -> io::Result<winapi::HANDLE> {
    let name = b"CONOUT$\0";
    let handle = unsafe {
        kernel32::CreateFileA(name.as_ptr() as *const i8,
                              winapi::GENERIC_READ | winapi::GENERIC_WRITE,
                              winapi::FILE_SHARE_WRITE,
                              ptr::null_mut(),
                              winapi::OPEN_EXISTING,
                              0,
                              ptr::null_mut())
    };
    if handle == winapi::INVALID_HANDLE_VALUE {
        Err(io::Error::last_os_error())
    } else {
        Ok(handle)
    }
}
// ----------------------------------------------------------------------------
fn set_col(foreground: Col) -> io::Result<()> {
    let handle = conout()?;
    let mut accum: winapi::WORD = 0;
    accum |= color_to_bits(foreground);
    unsafe {
        let mut buffer_info = ::std::mem::MaybeUninit::uninit();
        if kernel32::GetConsoleScreenBufferInfo(handle, buffer_info.as_mut_ptr()) != 0 {
            let buffer_info = buffer_info.assume_init();

            // leave background as it is
            accum |= buffer_info.wAttributes & 0b1111_0000;

            kernel32::SetConsoleTextAttribute(handle, accum);
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
impl Console for WinConsole {
    fn set_color(col: Col) {
        let result = match col {
            Col::Yellow => set_col(Col::Yellow),
            Col::Red => set_col(Col::Red),
            Col::Blue => set_col(Col::Blue),
            Col::Green => set_col(Col::Green),
            Col::Magenta => set_col(Col::Magenta),
            Col::Cyan => set_col(Col::Cyan),
            _ => set_col(Col::White),
        };

        if let Err(err) = result {
            panic!("failed setting console text attribute: {}", err);
        }
    }
}
// ----------------------------------------------------------------------------